/*  Mark Mandocdoc, Malik McElroy, Xiaojia Song, Mohamed Sharif
    masc0882
    Team California
    prog4
    CS530, Spring 2016
*/

#include "sicxe_asm.h"

using namespace std;

int main(int argc, char *argv[]) {

	if (argc != 2) {
		cout << "Error. Filename must be given at command line." << endl;
		exit(1);
	}

	string filename = argv[1];
	string raw_filename = get_raw_filename(filename);

	// initialize parsed file data
	//the_data.init_filename(filename);

	// initialize parsed file data
	file_parser the_data(filename);

	// initialize symtab
	symtab symtab_data;

	// initialize opcode table
	opcodetab opcode_table;

	// required to keep track of i
	// and display if not found in opcode
	int opcodetab_error_i = 0;
	
	try {
		
		// initialize file parser with given file name	
		the_data.read_file();

		// int array holding all the addresses
		unsigned int addresses[the_data.size()];

		// vector string containing the machine code hex
		vector <string> machine_code;	
	
		// location counter
		unsigned int locctr = 0;

		// global base register used for base calculations
		unsigned int base_register = 0;		

		bool start_encountered = false, end_encountered = false;
		bool base_enabled = false;
		string label, opcode, operand, comment;
		
		// the label used with the start directive
		// will be compared once the end directive is reached
		string start_label;
		
		// forward reference vector
		// will add new int i of line if a forward reference is required
		// then a middle loop after pass 1 will loop through the vector
		// forward_for contains the label that the forward reference is for
		// forward_is contains the label that is the forward reference
		vector <string> forward_for;
		vector <string> forward_is;
	
		for(int i=0; i<the_data.size(); i++) {

			// required for display error message
			opcodetab_error_i = i;

			label = the_data.get_token(i,0);
			opcode = the_data.get_token(i,1);
			operand = the_data.get_token(i,2);
			comment = the_data.get_token(i,3);
			transform(opcode.begin(), opcode.end(),opcode.begin(), ::toupper);

			// if label is not empty, add label to symtab
			// pass string label, int address, and int line number for exception
			// and also if its not the EQU directive
			if(!label.empty() && get_uppercase(opcode) != "EQU") symtab_data.add_symbol_to_map(label, locctr, i+1);

			if(opcode == "START") {
				start_encountered = true;
				addresses[i] = locctr;

				locctr = handle_start(operand, i);

				// initialize label found at start directive
				start_label = get_uppercase(label);

				continue;
			}
			else if(is_blank_or_comment(label,opcode,operand,comment)) { //removed !start_encountered
				addresses[i] = locctr;
				continue;
			}	
			else if(start_encountered && opcode == "START") {
				throw file_parse_exception(string_and_int("Cannot have two START directives on line ", i+1));
			}
			else if(!start_encountered && !is_blank_or_comment(label,opcode,operand,comment)) { 
				throw file_parse_exception(string_and_int("Non-empty line found before START directive on line ", i+1));
			}
			else if(opcode == "BYTE") {
				addresses[i] = locctr;
				locctr += handle_byte(operand, i);
				continue;	
			}	
			else if(opcode == "WORD") {
				addresses[i] = locctr;
				locctr += 3;
				continue;
			}
			else if(opcode == "BASE") {
				addresses[i] = locctr;
				base_enabled = true;	
				continue;
			}
			else if(opcode == "NOBASE") {
				addresses[i] = locctr;
				base_enabled = false;	
				continue;
			}
			else if(opcode == "RESB" || opcode == "RESW") {
				addresses[i] = locctr;
				locctr += add_bytes(opcode,operand, i);
				continue;
			}
			else if(opcode == "END") {
				end_encountered = true;
				addresses[i] = locctr;

				// compared end label with start label
				// throw exception if not the same
				if(get_uppercase(operand) != get_uppercase(start_label))
					throw file_parse_exception(string_and_int("End label does not match the start label on line ", i+1));

				continue;
			}
			else if(opcode == "EQU") {
				addresses[i] = locctr;
				symtab_data = handle_equ(label,operand,i,symtab_data);

				// check if forward reference
				if(label.empty()) {
					throw file_parse_exception(string_and_int("No label found for EQU directive on line ", i+1));
				}
				else if(operand.empty()) {
					throw file_parse_exception(string_and_int("No operand found for EQU directive on line ", i+1));
				}
				else if(isalpha(operand[0])) {
					if(!symtab_data.symbol_exists(operand)) { // if symbol is a forward reference
						forward_for.push_back(get_uppercase(label));
						forward_is.push_back(get_uppercase(operand));
					}
				}
				continue;
			} else if(opcode.empty()) { // Added after P4 grade. Fixes empty opcode lookup error.
				addresses[i] = locctr;
			} else {
				addresses[i] = locctr;
				locctr += opcode_table.get_instruction_size(opcode);
			}
		}
		// check if start has ever been encountered
		// if not, throw and exception
		if(!start_encountered)
			throw file_parse_exception("No START directive was found.");

		// same as above but for the END directive
		if(!end_encountered)
			throw file_parse_exception("No END directive was found.");
		
		// starting forward reference handling
		// it will continiously loop through forward reference vectors
		// if a forward refernce has been resolved it is removed from the vector
		// if it is not resolved it moves on to the next string in the vector
		// if it has gone through the whole vector and has not removed one
		// throw an exception because each pass should resolve to a forward reference
		while (forward_for.size() > 0) {
			int f;
			for(f = 0; f < forward_for.size(); f++) {
				if(exists_in_vector(forward_is[f], forward_for)) continue;
				else if(symtab_data.symbol_exists(forward_is[f])) {
					int address_of_forward_reference = symtab_data.get_address_of_symbol(forward_is[f]);
					symtab_data.update_symbol_with_address(forward_for[f],address_of_forward_reference);
					forward_is.erase(forward_is.begin() + f);
					forward_for.erase(forward_for.begin() + f);
				}
			}
			if (f == forward_for.size())
				throw symtab_exception("Forward references do not have an ending reference.");
		}

		// starting machine code variables
		int instruction = 0;
		int code;

		int instruction_size;
		int reg1_value = 0, reg2_value = 0;
		char *ptr;
		char *buffer;

		// reinitialize base_enabled for pass two
		// used for the machine code processing
		base_enabled = false;

		// reinitialize opcode error i for pass two
		// used to keep track of i for opcode error message
		opcodetab_error_i = 0;

		for(int i=0; i < the_data.size(); i++) {

			// required to display opcode tab error message
			opcodetab_error_i = i;

			label = the_data.get_token(i,0);
			opcode = get_uppercase(the_data.get_token(i,1));
			operand = get_uppercase(the_data.get_token(i,2));
			comment = the_data.get_token(i,3);

			// reinitialize instruction to 0
			instruction = 0;

			// if the line does not generate machine code continue the loop
			if(!opcode_generates_machine_code(opcode) && opcode != "BASE" && opcode != "NOBASE") {
				machine_code.push_back("");
				continue;
			}

			buffer = strdup(operand.c_str());
			string reg1 = "", reg2 = "";

			if(is_an_assembler_directive(opcode)) {
				if(opcode == "EQU") {
					symtab_data = handle_equ(label,operand,i,symtab_data);
					machine_code.push_back("");
					continue;
				}
				else if(opcode == "WORD") {
					if(operand[0] == '$') {
						operand.erase(0,1);
						machine_code.push_back(format_hex(hex_to_int(operand), 6));
						continue;
					}
					instruction |= decimal_to_int(operand);	
					machine_code.push_back(format_hex(instruction,6));
					continue;
				}
				else if(opcode == "BYTE") {
					string raw_operand = the_data.get_token(i,2);
					string quote = get_value_in_quotes(raw_operand);
					if(operand[0] == 'C') {
						string byte_string = "";
						for(int pos = 0; quote[pos] != NULL; pos++) {
							int pos_hex = quote[pos];
							byte_string += format_hex(pos_hex,0);
						}
						machine_code.push_back(byte_string);
					}
					else if(operand[0] == 'X') {
						string byte_string = "";
						for(int pos = 0; quote[pos] != NULL; pos++) {
							string tmp = "";
							tmp.push_back(quote[pos]);
							byte_string += format_hex(hex_to_int(tmp),0);
						}
						machine_code.push_back(byte_string);
					}
					continue;
				}
				else if(opcode == "BASE") {
					string base_label = operand;
					if(symtab_data.symbol_exists(base_label)) {
						base_register = symtab_data.get_address_of_symbol(base_label);
						base_enabled = true;
					}
					else throw symtab_exception(string_and_int("Symbol "+base_label+" not found in SYMTAB on line ", i+1));
					machine_code.push_back("");
					continue;
				}
				else if(opcode == "NOBASE") {
					base_enabled = false;
					machine_code.push_back("");
					continue;
				}
				else {
					machine_code.push_back("");
					continue;
				}
			}

			instruction_size = opcode_table.get_instruction_size(opcode);
			code = hex_to_int(opcode_table.get_machine_code(opcode));
			if(instruction_size == 3 && opcode[0] == '+') instruction_size = 4;

			if(instruction_size == 1)

				machine_code.push_back(format_hex(code,0));

			else if(instruction_size == 2) {

				ptr = strtok(buffer, ",");
				while(*ptr != NULL) 
					reg1 += *ptr++;				
				if(opcode == "SVC") {
					if(reg1[0] == '$') {
						reg1.erase(0,1);
						reg1_value = hex_to_int(reg1);
					}
					else
						reg1_value = decimal_to_int(reg1);
					instruction = code << 8;
					instruction |= decimal_to_int(reg1) << 4;
					machine_code.push_back(format_hex(instruction,0));
					continue;
				}
				else if(opcode == "TIXR" || opcode == "CLEAR") {
					reg1_value = get_reg_value(reg1);	
					if(reg1_value == 10)
						throw file_parse_exception(string_and_int("Invalid register specified on line ",i+1));
					instruction = code << 8;
					instruction |= reg1_value << 4;
					machine_code.push_back(format_hex(instruction,0));
					continue;
				}
				reg2 = get_reg2(operand);
				if(opcode == "SHIFTR" || opcode == "SHIFTL") {
					reg1_value = get_reg_value(reg1);	
					if(reg1_value == 10)
						throw file_parse_exception(string_and_int("Invalid register specified on line ",i+1));
					if(!isdigit(reg2[0]))
						throw file_parse_exception(string_and_int("Invalid operand specified on line ",i+1));
					reg2_value = decimal_to_int(reg2);
					instruction = code << 8;
					instruction |= reg1_value << 4;
					instruction |= reg2_value-1;
				}		
				else {
					reg1_value = get_reg_value(reg1);	
					reg2_value = get_reg_value(reg2);	
					if(reg1_value == 10 || reg2_value == 10)
						throw file_parse_exception(string_and_int("Invalid register specified on line ",i+1));
					instruction = code << 8;
					instruction |= reg1_value << 4;
					instruction |= reg2_value;
				}

				machine_code.push_back(format_hex(instruction,0));

			} else {

				// change string code to int opcode_hex	
				int opcode_hex = code; 

				// set the left most bits to the opcode hex
				instruction = opcode_hex << 16;
	
				bool is_constant = false;

				// initialize offset variables
				int dest_address = 0;
				int src_address = 0;
				int offset = 0;

				// loop for the RSUB exception, toggle bits, move on
				if(opcode == "RSUB") {
					instruction = toggle_bit(0, instruction);
					instruction = toggle_bit(1, instruction);
					machine_code.push_back(format_hex(instruction,0));
					continue;
				}

				// calculate offset to check if pc or base
				string dest_label = get_label_from_operand(operand, i);
				if(symtab_data.symbol_exists(dest_label)) {
					dest_address = symtab_data.get_address_of_symbol(dest_label);
					src_address = addresses[i+1];
					offset = dest_address - src_address;
				}
	
				// check operand and toggle bits N or I depending on whats found
				if(operand.empty()) {
					throw file_parse_exception(string_and_int("Invalid operand when setting NIXBPE bits on line  ", i+1));
				}
				else if(operand[0] == '#') {
					instruction = toggle_bit(1,instruction);
					if(isdigit(operand[1])) is_constant = true;
				} 
				else if(operand[0] == '@') {
					instruction = toggle_bit(0,instruction);	
				}
				else {
					instruction = toggle_bit(0, instruction);
					instruction = toggle_bit(1, instruction);
				}

				// set x bit if comma found
				if(operand.find(',') != string::npos) {
					instruction = toggle_bit(2, instruction);
				}

				// set e bit if format 4
				if(opcode[0] == '+') {
					instruction = toggle_bit(5, instruction);
				}
				else if (is_constant == false) {
					if(is_pc(offset)) {
						instruction = toggle_bit(4, instruction);
					}
					else {
						instruction = toggle_bit(3, instruction);
					}
				}

				// end nixbpe and begin address or displacement
				if(instruction_size == 3) {
					if(operand[0] == '#' && isdigit(operand[1])) {
						operand.erase(0,1);
						int value = decimal_to_int(operand);
						instruction |= value;
					}
					else if(operand[0] == '#' && operand[1] == '$') {
						operand.erase(0,2);
						int value = hex_to_int(operand);
						if (value <= 0xFFF)
							instruction |= value;
						else
							throw file_parse_exception(string_and_int("Immediate hex value greater than 0xFFF found on line ", i+1));
					} else {
						if(is_pc(offset)) {
							if(offset < 0) {
								//get the last 3 bytes of data from offset
								offset = offset & ((1 << 12) - 1);
							}
							instruction |= offset;
						} else {
							if(base_enabled) {
								int base_offset = dest_address - base_register;
								if(base_offset < 0)
									throw file_parse_exception(string_and_int("A negative base offset was found on line ", i+1));
								else if(base_offset > 4095)
									throw file_parse_exception(string_and_int("Base offset greater than 4095 found on line ", i+1));
								else
									instruction |= base_offset;
							} else throw file_parse_exception(string_and_int("Base relative addressing required but base is not enabled on line ", i+1));
						}
					}	
					machine_code.push_back(format_hex(instruction,6));
				}
				else if(instruction_size == 4) {	
					if(operand[0] == '#' && isdigit(operand[1])) {
						operand.erase(0,1);
						int value = decimal_to_int(operand);
						instruction <<= 8;
						instruction |= value;
					}
					else if(operand[0] == '#' && operand[1] == '$') {
						operand.erase(0,2);
						int value = hex_to_int(operand);
						instruction <<= 8;
						if (value < 0xFFFFF)
							instruction |= value;
						else
							throw file_parse_exception(string_and_int("Immediate hex value for format 4 instruction greater than 0xFFFFF found on line ", i+1));
					} else {
						string operand_label = get_label_from_operand(operand, i);
						operand_label = get_uppercase(operand_label);
						if(symtab_data.symbol_exists(operand_label)) {
							int dest_address = symtab_data.get_address_of_symbol(operand_label);
							instruction <<= 8;
							instruction |= dest_address;
						}
						else throw symtab_exception(string_and_int("Symbol "+operand_label+" not found in SYMTAB on line ", i+1));
					}
					machine_code.push_back(format_hex(instruction,8));
				}
		
			}

		}

		write_to_file(raw_filename, the_data, addresses, machine_code);
	}

	catch (file_parse_exception excpt) {

		cout << excpt.getMessage() << endl;
		exit(1);

	}
	catch (opcode_error_exception e) {
	
		cout	<< e.getMessage()
			<< string_and_int(" on line ", opcodetab_error_i + 1) << endl;
		exit(1);

	}
	catch (symtab_exception e) {
		cout << e.get_message() << endl;
		exit(1);
	}
	return 0;
}

void print_machine_code(file_parser the_data, vector <string> machine_code) {

	for(int i = 0; i < machine_code.size(); i++) {

		string instruction = machine_code[i];
		cout << string_and_int("Line:", i+1) << " " << instruction << endl;

	}

}

string get_value_in_quotes(string quote) {

	string value = "";
	bool inQuote = false;

	int i = 0;
	while(quote[i] != NULL) {
		if(quote[i] == '\'' && inQuote == true) break;
		if(inQuote) value.push_back(quote[i]);
		if(quote[i] == '\'' && inQuote == false) inQuote = true;
		i++;
	}

	return value;

}

string get_raw_filename(string filename) {

	char file_buffer[100];
	strcpy(file_buffer, filename.c_str());
	char* ptr = strtok(file_buffer,".");
	return ptr;

}

bool is_blank_or_comment(string lb, string op, string oper, string comm) {
         bool blank_or_comment = true;
 
        if(!lb.empty()) {
                if(lb[0] != '.') { 
			return false; 
		}
		else { 
			return true; 
		}
	}
        if(!op.empty()) {
                if(op[0] != '.') { 	
			return false; 
		}
		else { 
			return true; 
		}
	}
        if(!oper.empty()) {
                if(oper[0] != '.') { 
			return false; 
		} 
		else { 
			return true; 
		}
	}
        if(!comm.empty()) {
                if(comm[0] != '.') { 
			return false; 
		} 
		else { 
			return true; 
		}
	}

	return blank_or_comment;
}


bool is_blank(string lb, string op, string oper, string comm) {
	return (lb.empty() && op.empty() && oper.empty() && comm.empty());
}


unsigned int handle_start(string oper, int i) {
	if(oper.empty()) {
		throw file_parse_exception(string_and_int("No operand found for START directive on line ", i+1));
	}
	else if(oper[0] == '$') {
		oper.erase(0,1);
		return hex_to_int(oper);
	}
	else if(oper[0] == '#') {
		oper.erase(0,1);
		return decimal_to_int(oper);
	}
	else if(isdigit(oper[0])) {
		return decimal_to_int(oper);
	}
	else
		throw file_parse_exception(string_and_int("START directive operand is not a hexadecimal value on line ", i+1));
}

symtab handle_equ(string l, string oper, int i, symtab s) {
	int value;
	if(l.empty()) {
		throw file_parse_exception(string_and_int("No label found for EQU directive on line ", i+1));
	}
	else if(oper.empty()) {
		throw file_parse_exception(string_and_int("No operand found for EQU directive on line ", i+1));
	}
	else if(isalpha(oper[0])) {
		if(!s.symbol_exists(oper)) // if symbol is a forward reference
		s.add_symbol_to_map(l, 0, i); // add into symtab with value 0, update later in pass 2
	}
	else if(oper[0] == '$') {
		oper.erase(0,1);
		value = hex_to_int(oper);
		s.add_symbol_to_map(l, value, i);
	}
	else if(oper[0] == '#') {
		oper.erase(0,1);
		value = decimal_to_int(oper);
		s.add_symbol_to_map(l, value, i);
	}
	else if(isdigit(oper[0])) {
		value = decimal_to_int(oper);
		s.add_symbol_to_map(l, value, i);
	}
	else
		throw file_parse_exception(string_and_int("Invalid operand found for EQU directive on line ", i+1));

	return s;
}

int handle_byte(string s, int i) {

	if(s.empty())
		throw file_parse_exception(string_and_int("No operand found for BYTE directive on line ", i+1)); 

	int count = 0;
	bool in_quotes = false;
	
	if(tolower(s[0]) == 'x') {
		for(int i=0; i<s.size(); i++) {
			if(s[i] == '\'' && !in_quotes)
				in_quotes = true;
			else if(s[i] == '\'' && in_quotes)
				in_quotes = false;
			else if(s[i] == ' ' && in_quotes)
				// checks for white space in X byte directive and throws error
				throw file_parse_exception(string_and_int("Invalid SPACE character for X type BYTE directive on line ", i+1));
			else if(in_quotes)
				count++;
		}
	} else {
		for(int i=0; i<s.size(); i++) {
			if(s[i] == '\'' && !in_quotes)
				in_quotes = true;
			else if(s[i] == '\'' && in_quotes)
				in_quotes = false;
			else if(in_quotes)
				count++;
		}
	}
 
        if(tolower(s[0]) == 'x') {
                if(count % 2 != 0)
                       throw file_parse_exception(string_and_int("Odd number of characters for BYTE directive on line ", i+1)); 
                return count / 2;
        }
        else if(tolower(s[0]) == 'c')
                return count;
        else
                throw file_parse_exception(string_and_int("Operand is not hex or char on line ", i+1));
}

unsigned int hex_to_int(string l) {
	unsigned int integer_val;
	stringstream ss;
	ss << hex << l;
	ss >> integer_val;
	return integer_val;
}

unsigned int add_bytes(string op, string oper, int i) {
	if(oper.empty())
		throw file_parse_exception(string_and_int("No operand found for RESB or RESW directive on line ", i+1));
	if(op == "RESW") {
		if(oper[0] == '$')
			return hex_to_int(oper) * 3;
		else if(isdigit(oper[0]))
			return decimal_to_int(oper) * 3;
		else if (oper[0] == '#')
			return decimal_to_int(oper) * 3;
		else
			throw file_parse_exception(string_and_int("Invalid operand for RESB or RESW directive on line ", i+1));
	}
	else if(op == "RESB") {
		if(oper[0] == '$')
			return hex_to_int(oper);
		else if(isdigit(oper[0]) || oper[0] == '#')
			return decimal_to_int(oper);
		else
			throw file_parse_exception(string_and_int("Invalid operand for RESB or RESW directive on line ", i+1));
	}
	return 0;
}

int decimal_to_int(string l) {
	int integer_val = atoi(l.c_str());
	return integer_val;
}

// write data to file
void write_to_file(string filename, file_parser p, unsigned int* a, vector <string> machine_code) {

	// initialize variables
	string title = "**"+filename+".asm**";
	int max_width = 57;
	int setw_offset = (max_width/2) + (title.length()/2);
	string label, opcode, operand, comment;
	string lis_file = filename+ ".lis";

	// required to check if START has been passed
	bool has_started = false;

	// initialize file variable and create file
	ofstream file(lis_file.c_str(), fstream::out);
					
	file 	<< "\n"
		<< setw(setw_offset) << right << title << endl << endl;

	file	<< left << "Line#" 
		<< setw(13) << right << "Address"
		<< setw(13) << right << "Label"
		<< setw(13) << right << "Opcode"
		<< setw(12) << right << "Operand"
		<< setw(17) << right << "Machine Code" << endl; 

	file	<< left << "=====" 
		<< setw(13) << right << "======="
		<< setw(13) << right << "====="
		<< setw(13) << right << "======"
		<< setw(12) << right << "======="  
		<< setw(17) << right << "============" << endl;

	for (int i = 0; i < p.size(); i++ ) {

		label = get_uppercase(p.get_token(i,0));
		opcode = get_uppercase(p.get_token(i,1));
		operand = get_uppercase(p.get_token(i,2));
		comment = get_uppercase(p.get_token(i,3));

		if(has_started == false && is_blank_or_comment(label, opcode, operand, comment)) continue;
		if(opcode == "START") has_started = true;
		if(operand == "MACHINE") operand = "";

		if(operand.empty()) operand = "";	
	
		file 	<< setw(5) << right << dec << i+1
			<< setw(8) << right << ""; 
		file 	<< setw(5) << hex << uppercase << setfill('0') << a[i] << setfill(' ');
		file	<< setw(13) << right << label
			<< setw(7) << left << ""
			<< setw(11) << left << opcode
			<< setw(12) << left << operand
			<< setw(16) << left << machine_code[i] << endl;
		
	}

	
	// close file when done
	file.close();
}

// return capitalized string
string get_uppercase(string text) {

	transform(text.begin(), text.end(), text.begin(), ::toupper);
	return text;

}

// combines a string and int and returns a string
// used primarily for the exceptions and line numbers
string string_and_int(string s, unsigned int l) {

	std::ostringstream e;
	e << s << l;
	return e.str();

}

// code from class. takes in int and digits
// returns a string hex with leading zeros
string format_hex(int n, int digits) {

	stringstream s;
	s << hex << setw(digits) << setfill('0') << n;
	string toReturn = s.str();
	toReturn = get_uppercase(toReturn);
	return toReturn;

}

// return the symbol in the operand field
string get_label_from_operand(string operand, int i) {

	int value = 0;

	if(operand.empty()) {
		throw file_parse_exception(string_and_int("Invalid operand when extracting the label on line ", i+1));
	}
	else if(operand[0] == '$') {
		operand.erase(0,1);
		value = hex_to_int(operand);
	}
	else if(operand[0] == '#' || operand[0] == '@') {
		operand.erase(0,1);
	}
	else if(isdigit(operand[0])) {
		value = decimal_to_int(operand);
	}

	// check for comma
	if(operand.find(',') != string::npos){

		// get position of comma
		size_t comma_pos = operand.find(',');

		// get from position 0 to comma of string
		operand = operand.substr(0, comma_pos);

	}

	return operand;
}

// compare addresses and return if pc relative
bool is_pc(int offset) {

	if(offset > 2047 || offset < -2047) return false;
	else return true;

}

// checks if it is an assembly directive exception
bool is_asstab_exception(string opcode) {
	if(opcode == "WORD" || opcode == "BYTE") return true;
	else return false;
}

// compares given string opcode with the list
// of assembler directives on the header file
bool is_an_assembler_directive(string opcode) {
 
	static string const ass[] = {"START", "END", "BYTE", "WORD", "RESB", "RESW", "BASE", "NOBASE", "EQU"}; 
	static vector <string> asstab (ass, ass + sizeof(ass) / sizeof(ass[0]));

 	if( find(asstab.begin(), asstab.end(), opcode) == asstab.end()) return false;
 	return true;
 
}

// compares given string opcode with the list
// of assembler directives on the header file
bool opcode_generates_machine_code(string opcode) {

	if( opcode.empty() ) return false; 
	if( is_an_assembler_directive(opcode) ) {

		if( opcode == "WORD" || opcode == "BYTE" ) return true;
		else return false;

	}
	
	return true;
 
}

int toggle_bit(int bit, int instruction) {
	
	switch(bit) {

		case 0: // N-bit operations
			instruction |= nbit;		
			break;
		case 1: // I-bit operations
			instruction |= ibit;
			break;
		case 2: // X-bit operations
			instruction |= xbit;
			break;
		case 3: // B-bit operations
			instruction |= bbit;
			break;
		case 4: // P-bit operations
			instruction |= pbit;
			break;
		case 5: // E-bit operations
			instruction |= ebit;
			break;

	}
	
	return instruction;

}

int get_reg_value(string l) {
	if(l == "A")
		return 0;
	if(l == "X")
		return 1;
	if(l == "L")
		return 2;
	if(l == "B")
		return 3;
	if(l == "S")
		return 4;
	if(l == "T")
		return 5;
	if(l == "F")
		return 6;
	if(l == "PC")
		return 8;
	if(l == "SW")
		return 9;
	else
		return 10;
}

string get_reg2(string oper) {
	bool after_comma = false;
	string s = "";
	for(int i=0; i<oper.size(); i++) {
		if(oper[i] == ',') {
			after_comma = true;
			continue;
		}
		if(after_comma)
			s += oper[i];	
	}
	return s;
}

bool exists_in_vector(string needle, vector <string> haystack) {
	if(find(haystack.begin(), haystack.end(), needle) != haystack.end()) return true;
	return false;
}

