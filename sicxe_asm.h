/*  Mark Mandocdoc, Malik McElroy, Xiaojia Song, Mohamed Sharif
    masc0882
    Team California
    prog4
    CS530, Spring 2016
*/

#ifndef SICXE_ASM_H
#define SICXE_ASM_H

#include <algorithm>
#include <string>
#include <cstring>
#include <sstream> // required for string to integer conversion
#include <map>
#include <vector>
#include <iostream>
#include <fstream> // required for writing to file
#include "file_parser.h"
#include "file_parse_exception.h"
#include "opcodetab.h"
#include "opcode_error_exception.h"
#include "symtab.h"
#include "symtab_exception.h"

using namespace std;

#define nbit 0x20000
#define ibit 0x10000
#define xbit 0x8000
#define bbit 0x4000
#define pbit 0x2000
#define ebit 0x1000

int main (int, char** );

// returns file name without extension
string get_raw_filename(string);

// previews what will be printed into the list file
int print_file(string);

// handles specific cases
unsigned int handle_start(string, int);
int handle_byte(string, int);

// handles EQU directive and returns a symtab
symtab handle_equ(string,string,int,symtab);

// checks if the line is blank or has a comment
bool is_blank_or_comment(string,string,string,string);
bool is_blank(string, string, string, string);

// returns the int or hex value from a string
unsigned int hex_to_int(string);
int decimal_to_int(string);

// parses the operand and calculates bytes needed to add
unsigned int add_bytes(string, string, int);

// writes processed data to the lis file
void write_to_file(string, file_parser, unsigned int*, vector <string>);

// returns a capitalized string
string get_uppercase(string);
bool is_blank(string, string, string, string);

// takes a string and int and returns a string with both
string string_and_int(string, unsigned int);

// takes in an int n and int digits and returns string hex
string format_hex(int, int);

// return symbol from operand field
string get_label_from_operand(string, int);

// return true if dest - src is PC
bool is_pc(int);

// return true if the opcode is an assembler directive
bool is_an_assembler_directive(string);

int toggle_bit(int, int);

int get_reg_value(string);

string get_reg2(string);

// debugging function to view just machine code
void print_machine_code(file_parser, vector <string>);

// returns true if WORD or BYTE assembly directive exceptions
bool is_asstab_exception(string);

// returns true if the opcode generates a machine code
bool opcode_generates_machine_code(string);

// takes an operand and strips the value in quotes
// primarily used for BYTE directive and machine code creation
string get_value_in_quotes(string);

// returns true if string found in vector string
bool exists_in_vector(string, vector<string>);

#endif
