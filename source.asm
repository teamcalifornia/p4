. ASM file from the loader worksheet
. I wanted to type it up
. since it has correct machine code

prog	start	0
bsrch	equ	delta
offb	equ	8
	base	delta
	ldb	#delta
	lds	#$FFF
	+ldx	@foo
	+lda	beta,x
	add	gamma
	ldx	@zeta
first	norm
	+j	first
	clear	t
	+ldt	#$50000
	ldx	#0
	+jsub	bsrch
addl	lda	alpha,x
	add	beta,x
	sta	gamma,x
	addr	s,t
	compr	x,t
	+jlt	addl
	jlt	addl
	rsub


alpha	resw	48
beta	resw	16
gamma	resw	12
delta	word	20
zeta	byte	C'EOF'
eta	byte	x'fffffe'
theta	byte	c'eof'
buffer	resb	32
foo	word    1
	end	prog
