/*  Mark Mandocdoc, Malik McElroy, Xiaojia Song, Mohamed Sharif
    masc0882
    Team California
    prog4
    CS530, Spring 2016
*/

#ifndef SYMTAB_EXCEPTION_H
#define SYMTAB_EXCEPTION_H

#include <iostream>

using namespace std;

class symtab_exception {

	public:
		symtab_exception(string s) {
			message = s;
		}
			
		string get_message() {
			return message;
		}

	private:
		string message;	

};

#endif
